# Introdução 
O Tax Coupon Reader é uma aplicação Web que reconhece imagens de cupons fiscais.

# Componentes utilizados:
- react-easy-crop;
- get-orientation;
- axios;

# Arquitetura do Projeto:

- MVC;

## Comandos para executar o projeto:
- npm install
- npm start
- Baixar o projeto de backend em https://gitlab.com/gnalmeida/TaxCouponReader e seguir as intruções de execução;

