import axios from 'axios';

const ApiTaxCouponReader = axios.create({
    baseURL: process.env.REACT_APP_API_TAX_COUPON_READER,
});
export default ApiTaxCouponReader;