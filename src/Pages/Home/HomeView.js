import React from 'react';
import ImageUploadController from '../../Components/ImageUpload/ImageUploadController'
import Header from '../../Components/Header/Header'
import Footer from '../../Components/Footer/Footer'
import { Container, Row, Col } from 'react-bootstrap';
import './Home.css'

function HomeView(props) {
    return (
        <Container fluid={true}>
            <Row>
                <Header />
            </Row>
            <Row className="Content">
                <Col lg="12" xl="12" md="12" sm="12" xs="12">
                    <ImageUploadController />
                </Col>
                {/* <Col lg="6" xl="6" md="12" sm="12" xs="12">
                </Col> */}
            </Row>
            <Row >
                <Footer />
            </Row>
        </Container>
    );
}
export default HomeView;