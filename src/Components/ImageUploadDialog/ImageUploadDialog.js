import { React, useState } from 'react'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import CloseIcon from '@material-ui/icons/Close'
import Slide from '@material-ui/core/Slide'
import Button from '@material-ui/core/Button'
import { Spinner } from 'react-bootstrap';

const styles = {
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
  imgContainer: {
    position: 'relative',
    flex: 1,
    height: 500,
    padding: 16,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnRead: {
    position: 'relative',
    flex: 1,
    padding: 16,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    maxWidth: '100%',
    maxHeight: '100%',
  },
}

const ImageUploadDialog = (props) => {
  const { classes, uploadImage, onClose, img } = props;
  const [loading, setLoading] = useState(false);

  const handleUpload = async () => {
    setLoading(true);

    await uploadImage();

    setLoading(false);

    onClose();
  }

  return (
    <Dialog
      fullScreen
      open={!!img}
      onClose={onClose}
      TransitionComponent={Transition}
    >
      <div>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              onClick={onClose}
              aria-label="Close"
            >
              <CloseIcon />
            </IconButton>
            <Typography
              variant="title"
              color="inherit"
              className={classes.flex}
            >
              Imagem do Cupom
            </Typography>
          </Toolbar>
        </AppBar>
        <div className={classes.imgContainer}>
          <img src={img} alt="Cropped" className={classes.img} />
        </div>
        <div className={classes.btnRead}>
          {loading ?
            <div>
              <Spinner animation="border" variant="danger" />
            </div>
            :
            <Button
              onClick={handleUpload}
              variant="contained"
              color="primary"
            >
              Ler Cupom
            </Button>
          }
        </div>
      </div>
    </Dialog>
  )
}

function Transition(props) {
  return <Slide direction="up" {...props} />
}

export default withStyles(styles)(ImageUploadDialog);
