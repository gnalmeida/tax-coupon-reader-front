import React from 'react';
import './Header.css';
import { Container, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileUpload } from '@fortawesome/free-solid-svg-icons'

const Header = () => {
    return (
        <Container fluid={true} className="ContainerHeader">
            <Row>
                <Col className="TopHeader">
                    <FontAwesomeIcon icon={faFileUpload} /> Leitor de Cupons
                </Col>
            </Row>
        </Container>
    )
}
export default Header;