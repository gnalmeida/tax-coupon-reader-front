import React from 'react'
import Cropper from 'react-easy-crop'
import Slider from '@material-ui/core/Slider'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import ImageUploadDialog from '../ImageUploadDialog/ImageUploadDialog'
import CouponDialog from '../CouponDialog/CouponDialog'
import { ImageUploadStyles } from './ImageUploadStyles'
import { withStyles } from '@material-ui/core/styles'

const ImageUploadView = (props) => {

  const { imageSrc,
    rotation,
    setRotation,
    crop,
    setCrop,
    zoom,
    setZoom,
    croppedImage,
    onCropComplete,
    onClose,
    onFileChange,
    showCroppedImage,
    onCouponClose,
    coupon,
    uploadImage,
    classes } = props;

  return (
    <div>
      {imageSrc ? (
        <React.Fragment>
          <div className={classes.cropContainer}>
            <Cropper
              image={imageSrc}
              crop={crop}
              rotation={rotation}
              zoom={zoom}
              aspect={4 / 4}
              onCropChange={setCrop}
              onRotationChange={setRotation}
              onCropComplete={onCropComplete}
              onZoomChange={setZoom}
            />
          </div>
          <div className={classes.controls}>
            <div className={classes.sliderContainer}>
              <Typography
                variant="overline"
                classes={{ root: classes.sliderLabel }}
              >
                Zoom
              </Typography>
              <Slider
                value={zoom}
                min={1}
                max={3}
                step={0.1}
                aria-labelledby="Zoom"
                classes={{ root: classes.slider }}
                onChange={(e, zoom) => setZoom(zoom)}
              />
            </div>
            <div className={classes.sliderContainer}>
              <Typography
                variant="overline"
                classes={{ root: classes.sliderLabel }}
              >
                Rotation
              </Typography>
              <Slider
                value={rotation}
                min={0}
                max={360}
                step={1}
                aria-labelledby="Rotação"
                classes={{ root: classes.slider }}
                onChange={(e, rotation) => setRotation(rotation)}
              />
            </div>
            <Button
              onClick={showCroppedImage}
              variant="contained"
              color="primary"
              classes={{ root: classes.cropButton }}
            >
              Concluir
            </Button>
          </div>
          <ImageUploadDialog img={croppedImage} onClose={onClose} uploadImage={uploadImage} />
          <CouponDialog onClose={onCouponClose} coupon={coupon} />
        </React.Fragment>
      ) : (

        <div className={classes.fileUploadContainer}>
          <input type="file" onChange={onFileChange} accept="image/*" />
        </div>

      )}
    </div>
  )
}

export default withStyles(ImageUploadStyles)(ImageUploadView);
