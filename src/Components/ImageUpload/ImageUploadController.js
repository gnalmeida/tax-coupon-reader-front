import React, { useState, useCallback } from 'react'
import { getOrientation } from 'get-orientation/browser'
import { getCroppedImg, getRotatedImage } from '../../Utils/canvasUtils'
import ImageUploadView from './ImageUploadView'
import ApiTaxCouponReader from '../../Services/ApiTaxCouponReader';

const ORIENTATION_TO_ANGLE = {
    '3': 180,
    '6': 90,
    '8': -90,
}

const ImageUploadController = () => {
    const [imageSrc, setImageSrc] = React.useState(null)
    const [crop, setCrop] = useState({ x: 0, y: 0 })
    const [rotation, setRotation] = useState(0)
    const [zoom, setZoom] = useState(1)
    const [croppedAreaPixels, setCroppedAreaPixels] = useState(null)
    const [croppedImage, setCroppedImage] = useState(null)
    const [coupon, setCoupon] = useState(null)

    const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
        setCroppedAreaPixels(croppedAreaPixels)
    }, [])

    const showCroppedImage = useCallback(async () => {
        try {
            const croppedImage = await getCroppedImg(
                imageSrc,
                croppedAreaPixels,
                rotation
            )
            console.log('donee', { croppedImage })
            setCroppedImage(croppedImage)
        } catch (e) {
            console.error(e)
        }
    }, [imageSrc, croppedAreaPixels, rotation])

    const uploadImage = async () => {
        const originalFile = DataURIToBlob(imageSrc);
        const croppedFile = DataURIToBlob(croppedImage);

        const formData = new FormData();

        formData.append('files', originalFile, "originalFile");
        formData.append('files', croppedFile, "croppedFile");

        var response = await ApiTaxCouponReader.post('/TaxCoupon', formData, {
            headers: {
                'Content-Type': `multipart/form-data;`,
            }
        }).catch(() => { onCouponClose(); alert("Erro ao fazer upload"); });

        if (response.status === 200) {
            setCoupon(response.data);
        }
    }

    const onCouponClose = useCallback(() => {
        setCoupon(null);
        setCroppedImage(null);
        setImageSrc(null);
    }, [])

    const onClose = useCallback(() => {
        setCroppedImage(null);
    }, [])

    const onFileChange = async (e) => {
        if (e.target.files && e.target.files.length > 0) {
            const file = e.target.files[0]
            let imageDataUrl = await readFile(file)

            // apply rotation if needed
            const orientation = await getOrientation(file)
            const rotation = ORIENTATION_TO_ANGLE[orientation]
            if (rotation) {
                imageDataUrl = await getRotatedImage(imageDataUrl, rotation)
            }

            setImageSrc(imageDataUrl)
        }
    }

    return <ImageUploadView
        imageSrc={imageSrc}
        crop={crop}
        setCrop={setCrop}
        zoom={zoom}
        setZoom={setZoom}
        rotation={rotation}
        setRotation={setRotation}
        croppedImage={croppedImage}
        setCroppedImage={setCroppedImage}
        onCropComplete={onCropComplete}
        showCroppedImage={showCroppedImage}
        coupon={coupon}
        setCoupon={setCoupon}
        onCouponClose={onCouponClose}
        onClose={onClose}
        onFileChange={onFileChange}
        uploadImage={uploadImage}
    />;
}

function readFile(file) {
    return new Promise((resolve) => {
        const reader = new FileReader()
        reader.addEventListener('load', () => resolve(reader.result), false)
        reader.readAsDataURL(file)
    })
}

function DataURIToBlob(dataURI) {
    const splitDataURI = dataURI.split(',')
    const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
    const mimeString = splitDataURI[0].split(':')[1].split(';')[0]

    const ia = new Uint8Array(byteString.length)
    for (let i = 0; i < byteString.length; i++)
        ia[i] = byteString.charCodeAt(i)

    return new Blob([ia], { type: mimeString })
}

export default ImageUploadController;