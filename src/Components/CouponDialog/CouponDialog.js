import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import CloseIcon from '@material-ui/icons/Close'
import Slide from '@material-ui/core/Slide'
import { Container, Row, Col } from 'react-bootstrap';

const styles = {
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
  couponContainer: {
    position: 'relative',
    flex: 1,
    padding: 16,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    maxWidth: '100%',
    maxHeight: '100%',
  },
}

const CouponDialog = (props) => {
  const { classes, onClose, coupon } = props

  return (
    <Dialog
      fullScreen
      open={!!coupon}
      onClose={onClose}
      TransitionComponent={Transition}
    >
      <div>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              onClick={onClose}
              aria-label="Close"
            >
              <CloseIcon />
            </IconButton>
            <Typography
              variant="title"
              color="inherit"
              className={classes.flex}
            >
              Cupom Fiscal
            </Typography>
          </Toolbar>
        </AppBar>
        <div className={classes.couponContainer}>

          {coupon ? <>
            <Container>
              <Row>
                <Col>
                  Empresa: {coupon.companyName}
                </Col>
              </Row>
              <Row>
                <Col>
                  CNPJ: {coupon.cnpj}
                </Col>
              </Row>
              <hr />
              <Row>
                <Col>
                  Produtos:
                  <ul>
                    {coupon.products.map((itemProduct, i) => {
                      return <li key={i}>
                        {itemProduct}
                      </li>
                    })}
                  </ul>
                </Col>
              </Row>
              <hr />
              <Row>
                <Col>
                  Valor Total: {coupon.amount}
                </Col>
              </Row>
              <hr />
              <Row>
                <Col>
                  Reconhecido Por: {coupon.recognizedBy}
                </Col>
              </Row>
            </Container>

          </> : <></>}

        </div>
      </div>
    </Dialog>
  )
}

function Transition(props) {
  return <Slide direction="up" {...props} />
}

export default withStyles(styles)(CouponDialog);
