 import React from 'react';
 import './Footer.css';
 import { Container, Row, Col } from 'react-bootstrap';
 import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
 import { faFileUpload } from '@fortawesome/free-solid-svg-icons'
 
 const Footer = () => {
     return (
         <Container fluid={true} className="ContainerFooter">
             <Row>
                 <Col className="TextFooter">
                     <FontAwesomeIcon icon={faFileUpload} /> @2022 Leitor de Cupons - Todos os direitos reservados
                 </Col>
             </Row>
         </Container>
     )
 }
 export default Footer;